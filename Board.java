public class Board {
    private Die dieOne;
    private Die dieTwo;
    private boolean[] tiles;

    public Board() {
        this.dieOne = new Die();
        this.dieTwo = new Die();
        this.tiles = new boolean[12];
    }

    public String toString() {
        String result = "";
        for(int i=0; i < this.tiles.length; i++) {
            if(!this.tiles[i]) {
                result += " " + (i+ 1);
            }
            else {
                result += " " + "X";
            }
        }
        return result;

    }
    public boolean playATurn() {
        this.dieOne.roll();
        this.dieTwo.roll();
        System.out.println(dieOne.getFaceValue());
        System.out.println(dieTwo.getFaceValue());
        int sumOfDice = this.dieOne.getFaceValue() + this.dieTwo.getFaceValue();

            if(!this.tiles[sumOfDice-1]) {
                this.tiles[sumOfDice - 1] = true;
                System.out.println("Closing tile equal to sum:" + sumOfDice);
                return false;
            }
            else if(!this.tiles[this.dieOne.getFaceValue()-1]) {
                this.tiles[this.dieOne.getFaceValue()-1] = true;
                System.out.println("Closing tile with the same value as die one:" + this.dieOne.getFaceValue());
                return false;
            }

            else if(!this.tiles[this.dieTwo.getFaceValue()-1]) {
                this.tiles[this.dieTwo.getFaceValue()-1] = true;
                System.out.println("Closing tile with the same value as die two:" + this.dieTwo.getFaceValue());
                return false;
            }
            else {
                System.out.println("All the tiles for these\n" + "values are already shut");
                return true;
            }
    }
}