import java.util.Random;
public class Die {
    private int faceValue;
    private Random randomNumber;

    public Die() {
        this.faceValue = 1;
        this.randomNumber = new Random();
    }

    public int getFaceValue() {
        return this.faceValue;
    }

    public void roll() {
        this.faceValue = randomNumber.nextInt(6) + 1;
    }


    public String toString() {
        return "number rolled: " + this.faceValue;
    }
}