public class Jackpot {
    public static void main(String[] args) {

//        Die firstDice = new Die();
//        firstDice.roll();
//        System.out.println(firstDice.getFaceValue());
//        System.out.println(firstDice);

//        Die secondDice = new Die();
//        secondDice.roll();
//        System.out.println(secondDice.getFaceValue());
//        System.out.println(secondDice);

        Board myBoard = new Board();
        boolean gameOver = false;
        int numOfTilesClosed = 0;
        while(!gameOver) {
            System.out.println(myBoard);

            if(myBoard.playATurn()) {
                gameOver = true;

            }
            else {
                numOfTilesClosed++;
            }
        }
        if(numOfTilesClosed >= 7) {
            System.out.println("player reached the jackpot and won");
        }
        else {
            System.out.println("the player lost.");
        }

    }
}